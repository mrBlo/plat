package com.blo.plat.Controller;

import com.blo.plat.Model.NearByApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NearByApi {
    @GET("api/place/nearbysearch/json?sensor=true&key=AIzaSyAmj-dOzmjQDXSOeUBqfdLJdnmaaH5fPJY")
    Call<NearByApiResponse> getNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);

}
