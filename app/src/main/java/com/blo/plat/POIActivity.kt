package com.blo.plat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.blo.plat.Util.isOnline
import com.mancj.materialsearchbar.MaterialSearchBar

class POIActivity : AppCompatActivity() {
    val context = this@POIActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poi)

        //REFERENCE MATERIALSEARCHBAR AND LISTVIEW
        val lv = findViewById<ListView>(R.id.mListView)
        val searchBar = findViewById<MaterialSearchBar>(R.id.searchBar)
        searchBar.setHint("Search..")
        //searchBar.setSpeechMode(true)

        val galaxies = arrayOf(
            "ATMs",
            "Restaurants", "Bars", "Hotels",
            "Attractions", "Groceries", "Pharmacies", "Schools",
            "Hospitals",
            "Petrol Stations", "Gyms", "Nightlife", "Cinemas",
            "Parking", "Libraries", "Parks", "Museums",
            "Churches", "Mosques"
        )

        //ADAPTER
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, galaxies)
        lv.setAdapter(adapter)


        //SEARCHBAR TEXT CHANGE LISTENER
        searchBar.addTextChangeListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //SEARCH FILTER
                adapter.getFilter().filter(charSequence)
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })


        //LISTVIEW ITEM CLICKED
        lv.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                Toast.makeText(
                    context,
                    adapter.getItem(i)!!.toString(),
                    Toast.LENGTH_SHORT
                ).show()

                //CHECK INTERNET CONNECTIVITY
                val dataOn = isOnline(context)
                Log.e("-----", dataOn.toString())

                if (!dataOn) Toast.makeText(
                    context,
                    getString(R.string.INTERNET_CONNECTIVITY),
                    Toast.LENGTH_LONG
                ).show()
                else {
                    val intent = Intent(context, MapsActivity::class.java)
                    intent.putExtra("SELECTED_POI", adapter.getItem(i)!!.toString())
                    startActivity(intent)
                }

            }
        })


    }


}
