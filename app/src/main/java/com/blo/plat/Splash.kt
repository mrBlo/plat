package com.blo.plat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import gr.net.maroulis.library.EasySplashScreen


class Splash : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val easySplashScreenView = EasySplashScreen(this@Splash)
            .withFullScreen()
            .withTargetActivity(POIActivity::class.java)
            .withSplashTimeOut(2000)
            .withBackgroundResource(com.blo.plat.R.color.white)
            //.withHeaderText("Header")
            // .withFooterText("Loading..")
            //  .withBeforeLogoText("My cool company")
            .withLogo(com.blo.plat.R.drawable.pinlogo)
            //.withAfterLogoText("READ'E")
            .create()

        setContentView(easySplashScreenView)
        supportActionBar!!.hide()
    }
}
