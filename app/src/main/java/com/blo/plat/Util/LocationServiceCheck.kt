package com.blo.plat.Util

import android.content.Context
import android.location.LocationManager
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.location.LocationServices


private fun isLocationEnabled(context: Context): Boolean {
    var locationManager: LocationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
        LocationManager.NETWORK_PROVIDER
    )
}